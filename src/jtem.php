<?php

// Requires PHP Version 5.4 due to array initialization shorthand.

if(array_search("--help", $argv))
{
	// TODO: Handle help.
	echo "Need help.\n";
	exit(0);
}

if(array_search("--version", $argv))
{
	// TODO: Handle version.
	echo "Need version.\n";
	exit(0);
}

if(count($argv) > 1)
{
	$file = $argv[1];
}
else
{
	echo "No input contacts file was specified.  This needs to be the first argument, and\npoint to a file in vCard format.\n";
	exit(1);  // No input file specified.
}

if(file_exists($file))
{
	$records = explode("END:VCARD", file_get_contents($file));
	$flattened = [];

	foreach($records as $record)
	{
		$flattened = array_merge($flattened, flatten($record));
	}

	echo implode("\n", $flattened);
}
else
{
	echo "The input vCard file could not be found.\n";
	exit(2);  // Couldn't find input file.
}


function flatten($record)
{
	$fields = explode("\n", $record);
	$entries = [];

	$fn = field_fn($fields);
	$n = field_n($fields);

	foreach($fields as $field)
	{
		if(substr($field, 0, 6) == "EMAIL;")
		{
			$entries[] = "BEGIN:VCARD\nVERSION:3.0\n$fn\n$n\n$field\nEND:VCARD";
		}
	}

	return $entries;
}


function field_fn($fields)
{
	foreach($fields as $field)
	{
		if(substr($field, 0, 3) == "FN:")
		{
			return $field;
		}
	}
}


function field_n($fields)
{
	foreach($fields as $field)
	{
		if(substr($field, 0, 2) == "N:")
		{
			return $field;
		}
	}
}